package ru.kildishov.service;

import ru.kildishov.model.BankCard;
import ru.kildishov.repo.BankCardRepo;

public class CardService {
	
	BankCardRepo bankCardRepo = new BankCardRepo();
	
	public BankCard autorization(String cardNumber, String password) {
		
		return bankCardRepo.findByCardNumberAndByPassword(cardNumber, password);
		
	}
	
	public BankCard searchBankCardByNumber(String cardNumber) {
		
		return bankCardRepo.findByCardNumber(cardNumber);
		
	}
	
	public void showCardBalance(BankCard bankCard){
		System.out.println(bankCardRepo.showCardBalance(bankCard));
	}
	
	public void withdrawCash(Long cash, BankCard bankCard){
		bankCardRepo.withdrawCash(cash, bankCard);
	}
	
	public void depositCash(Long cash, BankCard bankCard){
		bankCardRepo.depositCash(cash, bankCard);
	}
	
	
	
	public void close(){
		bankCardRepo.writeObject();
	}

}
