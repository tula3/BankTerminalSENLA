package ru.kildishov.repo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import ru.kildishov.model.BankCard;

public class BankCardRepo {
	
	private ArrayList<BankCard> arrayBankCard = new ArrayList<BankCard>();
	
	public ArrayList<BankCard> getArrayBankCard() {
		return arrayBankCard;
	}

	public void setArrayBankCard(ArrayList<BankCard> arrayBankCard) {
		this.arrayBankCard = arrayBankCard;
	}

	public void readObject() {
		
		BufferedReader reader;
		try {
			
			reader = new BufferedReader(new FileReader("repo.txt"));
			String line = reader.readLine();
	
        while (line != null) {   
        	
        	BankCard bankCard = new BankCard();
        	
            
            String[] parts = line.split(" ");
            bankCard.setСardNumber(parts[0]);
            bankCard.setPassword(parts[1]);
            long cash = 0;
            cash = Integer.parseInt(parts[2]);
            bankCard.setCash(cash);
            
         
          
            
            arrayBankCard.add(bankCard);
            
            line = reader.readLine();
        }
        
        reader.close();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
    	e.printStackTrace();
    }
	}
	
	public BankCard findByCardNumberAndByPassword(String cardNumber, String password) {
		readObject();
		for(BankCard card : arrayBankCard) {
			if(card.getСardNumber().equals(cardNumber) & card.getPassword().equals(password) ) {

				return card;
			}
		}
		return null;
	}
	
	public BankCard findByCardNumber(String cardNumber) {
		readObject();
		for(BankCard card : arrayBankCard) {
			if(card.getСardNumber().equals(cardNumber) ) {

				return card;
			}
		}
		return null;
	}
	
	public Long showCardBalance(BankCard bankCard){
		

		for(BankCard card : arrayBankCard) {
			if(card.getСardNumber().equals(bankCard.getСardNumber()) ) {

				return  card.getCash();
			}
		}
	
		return null;
	
	}
	
	public void withdrawCash(Long cash, BankCard bankCard  ){
		
		for(BankCard card : arrayBankCard) {
			if(card.getСardNumber().equals(bankCard.getСardNumber()) ) {
				
				Long cashNew = card.getCash() - cash; 
				
				if(cashNew < 0){
					
					System.out.println("not enough cash");
					
				}else{
					
					card.setCash(cashNew);
				
				}
			
			}
		}
		
	}
	
	public void depositCash(Long cash, BankCard bankCard){
		
		for(BankCard card : arrayBankCard) {
			if(card.getСardNumber().equals(bankCard.getСardNumber()) ) {
				
				Long cashNew = card.getCash() + cash; 
				
				if(cashNew > 1000000){
					
					System.out.print("exceeded the limit");
					
				}else{
					
					card.setCash(cashNew);
				
				}
			
			}
		}
		
	}
	
	
	
	public void writeObject() {
		
	try {
		FileWriter fileWriter = new FileWriter("repo.txt");	
		
		for(BankCard card : arrayBankCard) {
			fileWriter.write(card.getСardNumber() + " " + card.getPassword() + " " +card.getCash() +"\n");
		}
		
		fileWriter.close();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
        System.out.print("An error occurred while writing th the file");
    } catch (IOException e) {
    	e.printStackTrace();
    }
		
		
	}
	
	
}
