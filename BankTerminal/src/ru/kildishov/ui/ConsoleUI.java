package ru.kildishov.ui;

import java.util.Scanner;

import ru.kildishov.model.BankCard;
import ru.kildishov.repo.BankCardRepo;
import ru.kildishov.service.CardService;

public class ConsoleUI {
	
	boolean exit = false;
	
	boolean wrongPassword = false;
	
	int countAttempts = 0; 
	
	
	
	BankCard bankCard = new  BankCard();
	
	BankCard searchbankCard = new BankCard(); 
	
	CardService cardService = new CardService();
	
	public void singIn() {
		System.out.print("Enter card number : ");
		Scanner scanLineCardNumber = new Scanner(System.in);
		String cardNumber = scanLineCardNumber.nextLine();
		
		while(!wrongPassword & countAttempts < 3){
			countAttempts++;
			System.out.print("Enter card password : ");
			Scanner scanLineCardPassword = new Scanner(System.in);
			String cardPassword = scanLineCardPassword.nextLine();
			
		
			bankCard = cardService.autorization(cardNumber, cardPassword);
			if(searchbankCard != null){
				break;
			}
			
			if(countAttempts >3 ){
				wrongPassword=true;
				
			}
		}			
		
		if(bankCard != null) {
			System.out.println("successfully");
			
			while(!exit) {
			System.out.println("Enter: 1 - check the card balance; 2 - withdraw cash; 3 - deposit cash; 4 - exit");
			Scanner scanChoice = new Scanner(System.in);
			int choice = scanChoice.nextInt();
			 if (choice == 1) {
				 	cardService.showCardBalance(bankCard);
	            } else if (choice == 2) {
	            	System.out.print("Enter amout of cash : ");
	        		Scanner scanLineCardWithdraw = new Scanner(System.in);
	        		Long amoutOfCash = scanLineCardWithdraw.nextLong();
	        		
	        		cardService.withdrawCash(amoutOfCash, bankCard);
	        		
	            } else if (choice == 3) {
	            	System.out.print("Enter amout of cash : ");
	        		Scanner scanLineCardWithdraw = new Scanner(System.in);
	        		Long amoutOfCash = scanLineCardWithdraw.nextLong();
	        		
	        		cardService.depositCash(amoutOfCash, bankCard);
	            	
	            } else if (choice == 4) {
	            	System.out.println("exit");
	                exit = true;
	                cardService.close();
	            }else {
	            	System.out.println("incorrect value! try again");
	            }
				
			}	
		}else{
			System.out.println("wrong card number or password");
		}
	}

}
