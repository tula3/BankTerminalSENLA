package ru.kildishov.model;

import java.io.Serializable;
import java.util.Date;

public class BankCard implements Serializable{
	
	private String сardNumber;
	
	private String password;
	
	private Long cash;
	
	public BankCard() {
		
	}
	
	public BankCard(String сardNumber, String password, Long cash){
		this.cash= cash;
		this.сardNumber = сardNumber;
		this.password = password; 
	}

	public String getСardNumber() {
		return сardNumber;
	}

	public void setСardNumber(String сardNumber) {
		this.сardNumber = сardNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getCash() {
		return cash;
	}

	public void setCash(Long cash) {
		this.cash = cash;
	} 

}
