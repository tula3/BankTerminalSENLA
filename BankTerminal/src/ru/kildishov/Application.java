package ru.kildishov;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import ru.kildishov.model.BankCard;
import ru.kildishov.repo.BankCardRepo;
import ru.kildishov.ui.ConsoleUI;

public class Application {

	public static void main(String[] args) {

		ConsoleUI main = new ConsoleUI();
		main.singIn();
		
	}

}
